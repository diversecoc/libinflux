/*!
 * \file influx.h
 *
 * \brief API de communication avec InfluxDB
 *
 *	API client en écriture pour InfluxDB.
 *
 * "<measurement>[,<tag_key>=<tag_value>[,<tag_key>=<tag_value>]] <field_key>=<field_value>[,<field_key>=<field_value>] [<timestamp>]"
 *
 * @(#) $Id$ Ecoc
 *
 * \author Ecochard Jean-Paul <jp.ecoc@free.fr>
 * \date   ven. mars. 2 18:54:24 CET 2018
 */

#ifndef INFLUX_H_
#define INFLUX_H_

#include <inttypes.h>
#include <stddef.h>

typedef struct _InfluxDBClient_ InfluxDBClient;
typedef enum precision {
	NS = 9,							/*!< Timestamps in nanoseconds (defaults). */
	US = 6,							/*!< Timestamps in microseconds. */
	MS = 3,							/*!< Timestamps in milliseconds. */
	S  = 0							/*!< Timestamps in seconds. */
}	InfluxDB_Prec_t;

InfluxDBClient* InfluxDBClientNew();
void InfluxDBClientDestroy( InfluxDBClient* clt );

void InfluxDB_setConnectionParamsV2( InfluxDBClient* clt, char* url, char* org, char* bucket, char* token );
void InfluxDB_setConnectionParamsV1( InfluxDBClient* clt, char* url, char* user, char* pass, char* base );
void InfluxDB_setPrecision( InfluxDBClient* clt, InfluxDB_Prec_t precision );
void InfluxDB_setMinMeasures( InfluxDBClient* clt, size_t min );
void InfluxDB_setMaxInterval( InfluxDBClient* clt, size_t max );
void InfluxDB_addGlobalTag( InfluxDBClient* clt, char* nom, char* valeur );
int InfluxDB_GlobalTag_empty( InfluxDBClient* clt );

void InfluxDB_addTag( InfluxDBClient* clt, char* nom, char* valeur );
void InfluxDB_addTagUInt( InfluxDBClient* clt, char* nom, uint64_t valeur );
void InfluxDB_addTagInt( InfluxDBClient* clt, char* nom, int64_t valeur );
void InfluxDB_addTagDouble( InfluxDBClient* clt, char* nom, double valeur );

void InfluxDB_addField( InfluxDBClient* clt, char* nom, char* valeur );
void InfluxDB_addUIntField( InfluxDBClient* clt, char* nom, uint64_t valeur );
void InfluxDB_addIntField( InfluxDBClient* clt, char* nom, int64_t valeur );
void InfluxDB_addDoubleField( InfluxDBClient* clt, char* nom, double valeur );
void InfluxDB_addMeasure( InfluxDBClient* clt, char* nom );

const char const* InfluxDB_to_string( InfluxDBClient* clt );
int InfluxDB_write( InfluxDBClient* clt );
const char const* InfluxDB_get_response( InfluxDBClient* clt, size_t* size );

int InfluxDB_postQuery( InfluxDBClient* clt, char* requete, char** resp );
int InfluxDB_getQuery( InfluxDBClient* clt, char* requete, char** resp );

#endif /* INFLUX_H_ */
