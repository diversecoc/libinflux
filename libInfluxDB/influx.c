/*!
 * \file influx.c
 *
 * \brief API de communication avec InfluxDB
 *
 *	API client en écriture pour InfluxDB
 *
 * "<measurement>[,<tag_key>=<tag_value>[,<tag_key>=<tag_value>]] <field_key>=<field_value>[,<field_key>=<field_value>] [<timestamp>]"
 *
 * @(#) $Id$ Ecoc
 *
 * \author Ecochard Jean-Paul <jp.ecoc@free.fr>
 * \date   ven. mars. 2 18:54:24 CET 2018
 */
/*!
 * \mainpage Influx_API Interface client pour InfluDB
 *
 *	\section intro_sec Introduction
 *
 *	Cette librairie implémente une interface de communication vers InfluxDB en version 1 et version 2.
 *
 *	\section making Compilation
 *
 *	\subsection deps Dépendances
 *
 *	Les librairies suivantes sont indispensables à la compilation :
 *
 *		- CURL			Communication HTTP/HTTPS (https://github.com/curl/curl).
 *		- GLIB			GNOME library			 (https://github.com/GNOME/glib).
 *		- linvpair		Gestion des paires nom=valeur.
 *
 *	\subsection make Compilation
 *
 *	La séquence habituelle,
 *
 *	configure --prefix=la/ou/je/veux && make
 *
 *	doit produire la librairie.
 *
 *	Une commande make check compile les tests éventuels.
 *
 *	\section install_sec Installation
 *
 *	Après compilation, l'installation s'effectue par make install dans le répertoire indiqué par l'option --prefix lors
 *	de l'étape précédente.
 *
 *	\section use Utilisation
 *
 *	\code{.c}
 *
 *	#include "influx.h"
 *
 *	#define URL "http://localhost:8086"
 *	#define DB	 "testamoi"
 *
 *  int main( int argc, char* argv[] )
 *  {
 *  	// Create connection
 *  	//
 *  	InfluxDBClient* clt = InfluxDBClientNew();
 *		setConnectionParamsV1( clt, URL, NULL, NULL, DB );
 *		//
 *		//	Create comon tags
 *		//
 *		InfluxDB_addGlobalTag( clt, "host", "host1" );
 *		InfluxDB_addGlobalTag( clt, "type", "vm" );
 *		//
 *		//	Create some fields
 *		//
 *		InfluxDB_addDoubleField( clt, "load1m", 5.65 );
 *		InfluxDB_addDoubleField( clt, "load5m", 4.32 );
 *		InfluxDB_addDoubleField( clt, "load15m",2.10 );
 *		InfluxDB_addIntField( clt, "nb_cpu", 8 );
 *		//
 *		//	Add measurement "system"
 *		//
 *		InfluxDB_addMesure( clt, "system" );
 *		.......
 *		//
 *		// Force send of remaining datas
 *		//
 *		InfluxDBClientWrite( clt );
 *
 *		InfluxDBClientDestroy( clt );
 *
 *	    return 0;
 *	}
 *
 *	\endcode
 *
 */
#include <stdio.h>
#include <glib.h>
#include <gmodule.h>
#include <curl/curl.h>
#include "libnvpair.h"

#include "helpers.h"
#include "influx.h"

typedef struct chunk {
	char*	memory;
	size_t	size;
} chunk;
/*!
 * \brief	New InfluxDB client.
 */
typedef struct _InfluxDBClient_ {
	CURL*			curl;				/*!< Handler de communication. */
	CURLcode		curlc;				/*!< Last error */
	struct chunk*	chunks;				/*!< Memory buffer for HTTP exchanges. */

	nvlist_t*		tags_commun;		/*!< Tags commun à toutes les mesures ajoutées. */
	nvlist_t*		tags;				/*!< Spécific tag list. */
	nvlist_t*		fields;				/*!< Measurement fields. */
	GString*		buffer;				/*!< Measurement buffer. */
	uint64_t		nb_mesures;			/*!< Measurement number. */
	time_t			last_send;			/*!< Time of last write. */

	InfluxDB_Prec_t	precision;			/*!< Timestamp unit. */

	enum version {
		v1 = 1,
		v2
	}		version;					/*!< Version de InfluxDB.*/
	char*			urlp;				/*!< URL handler */
	char*			user_or_org;		/*!< User (InfluxDB1) or organization (InfluxDB2) */
	char*			pass_or_token;		/*!< Password (InfluxDB1) or token (InfluxDB2) */
	char*			base_or_bucket;		/*!< Base (InfluxDB1) or bucket (InfluxDB2) */
	uint_t			min_mesures;		/*!< Nombre minimum de mesures avant envoi. */
	uint_t			max_temps;			/*!< Attente maximum avant envoi. */
} InfluxDBClient;

static int query( CURL* client, char* url, int method, GString* body,
		struct chunk* chunk, size_t (*cb)( void *contents, size_t size, size_t nmemb, void *userp ) );
/*!
 * \brief Fonction de rappel pour chaque requête.
 *
 *	Fonction de rappel qui permet d'accumuler les fragments reçus
 * pour chaque requête dans un buffer lié à une URL.
 *
 * @param contents	Le buffer reçu.
 * @param size		Taille du buffer.
 * @param nmemb		Nombre de membre.
 * @param userp		MemoryStruct fournie par l'utilisateur.
 *
 * @return	Taille du buffer accumulé.
 */
static size_t WriteMemoryCallback( void *contents, size_t size, size_t nmemb, void *userp )
{
	size_t realsize = size * nmemb;
	struct chunk *mem = (struct chunk *)userp;

	char *ptr = realloc( mem->memory, mem->size + realsize + 1 );
	if( !ptr ) {
		fprintf( stderr, "not enough memory (realloc returned NULL)\n" );
		return 0;
	}

	mem->memory = ptr;
	memcpy( &(mem->memory[mem->size]), contents, realsize );
	mem->size += realsize;
	mem->memory[mem->size] = 0;

	return realsize;
}
/*!
 * \brief	Création d'un client InfluxDB.
 *
 * @return	Un pointeur sur le nouveau client.
 */
InfluxDBClient* InfluxDBClientNew()
{
	InfluxDBClient* tmp = calloc( 1, sizeof(InfluxDBClient) );
	nvlist_alloc( &tmp->tags_commun, NV_UNIQUE_NAME, 0 );
	nvlist_alloc( &tmp->tags, NV_UNIQUE_NAME, 0 );
	nvlist_alloc( &tmp->fields, NV_UNIQUE_NAME, 0 );
	tmp->buffer = g_string_new(NULL);
	tmp->precision = NS;

	return tmp;
}
/*!
 * \brief	Destruction d'un client InfluxDB.
 *
 * @param clt	Client à détruire.
 */
void InfluxDBClientDestroy( InfluxDBClient* clt )
{
	if( !clt ) return;

	curl_easy_cleanup( clt->curl );
	clt->curl = NULL;

	if( clt->urlp )				free( clt->urlp );
	if( clt->user_or_org )		free( clt->user_or_org );
	if( clt->pass_or_token )	free( clt->pass_or_token );
	if( clt->base_or_bucket )	free( clt->base_or_bucket );

	clt->base_or_bucket = NULL;
	clt->pass_or_token = NULL;
	clt->user_or_org = NULL;
	clt->urlp = NULL;

	if( clt->chunks && clt->chunks->memory ) {
		free( clt->chunks->memory );
		clt->chunks->size = 0;
		clt->chunks->memory = NULL;
	}
	if( clt->chunks ) free( clt->chunks );
	g_string_free( clt->buffer, TRUE );

	nvlist_free( clt->tags_commun );
	nvlist_free( clt->tags );
	nvlist_free( clt->fields );

	free( clt );
}
/*!
 * \brief Free tags and fields lists.
 *
 * @param clt			Client InfluxDB.
 */
static void reset_tags_and_fields( InfluxDBClient* clt )
{
	if( !clt ) return;

	nvlist_free( clt->tags );
	nvlist_free( clt->fields );
	nvlist_alloc( &clt->tags, NV_UNIQUE_NAME, 0 );
	nvlist_alloc( &clt->fields, NV_UNIQUE_NAME, 0 );
}
/*!
 * \brief	Paramètres de connexion pour la version V2.
 *
 *	Fixe les paramètres de connexion à une base InfluxDB V2:
 *	- url: URL de connexion (défaut: http://localhost:8086"
 *	- org: Organisation (défaut: MyOrg)
 *	- bucket: Bucket qui stocke les données (défaut: MyBucket)
 *	- token: Jeton qui autorise l'écriture dans le bucket (défaut: "12345678890ABCDefghij")
 *
 *	Il y a aucune chance de bon fonctionnement avec les valeurs par défaut!
 *
 * @param clt		Client InfluxDB.
 * @param url		URL de connexion à InfluxDB (Ex: http://influx_host:8086/)
 * @param org		Organisation à utiliser.
 * @param bucket	Bucket pour stocker les points.
 * @param token		Token d'identification.
 */
void InfluxDB_setConnectionParamsV2( InfluxDBClient* clt, char* url, char* org, char* bucket, char* token )
{
	if( !clt ) return;

	clt->urlp = strdup( url == NULL || *url == '\0' ? "http://localhost:8086" : url );
	clt->pass_or_token = strdup( token == NULL || *token == '\0' ? "12345678890ABCDefghij" : token );
	clt->base_or_bucket = strdup( bucket == NULL || *bucket == '\0' ? "MyBucket" : bucket );
	clt->user_or_org = strdup( org == NULL || *org == '\0' ? "MyOrg" : org );

	clt->version = v2;
	clt->curl = curl_easy_init();

	clt->chunks = malloc( sizeof(struct chunk) );
	clt->chunks->memory = NULL;
	clt->chunks->size = 0;
}
/*!
 * \brief	Paramètres de connexion pour la version V1.
 *
 *	Fixe les paramètres de connexion à une base InfluxDB V2:
 *	- url: URL de connexion (défaut: http://localhost:8086"
 *	- user: Utilisateur (défaut: NULL = pas d'utilisateur)
 *	- pass: Mot de passe de l'utilisateur autorisé (défaut: NULL = pas de mot de passe)
 *	- base: Base qui stocke les données (défaut: MyBase)
 *
 *	Si l'utilisateur est NULL, le mot de passe est ignoré.
 *
 *	Il y a aucune chance de bon fonctionnement avec les valeurs par défaut!
 *
 * @param clt		Client InfluxDB.
 * @param url		URL de connexion à InfluxDB (Ex: http://influx_host:8086/)
 * @param user		Utilisateur de connexion à la base.
 * @param pass		Mot de passe pour la connexion à la base.
 * @param base		Base à utiliser.
 */
void InfluxDB_setConnectionParamsV1( InfluxDBClient* clt, char* url, char* user, char* pass, char* base )
{
	if( !clt ) return;

	int len = strlen( url );
	if( url[len-1] == '/' ) url[len-1] = '\0';

	clt->urlp = strdup( url == NULL || *url == '\0' ? "http://localhost:8086" : url );
	clt->base_or_bucket = strdup( base == NULL || *base == '\0' ? "MyBase" : base );
	clt->user_or_org = user == NULL || *user == '\0' ? NULL :  strdup( user );
	if( clt->user_or_org == NULL ) {
		clt->pass_or_token = NULL;
	} else {
		clt->pass_or_token = pass == NULL || *pass == '\0' ? NULL : strdup( pass );
	}

	clt->version = v1;
	clt->curl = curl_easy_init();

	clt->chunks = malloc( sizeof(struct chunk) );
	clt->chunks->memory = NULL;
	clt->chunks->size = 0;
}
/*!
 * \brief Sets timestamps precision.
 *
 *	La précision du "timestamp" est la nanoseconde par défaut. La précision à choisir doit être le plus
 * proche possible de la fréquence de collecte.
 *	Le "timestamp" est placé automatiquement lors de l'ajout de la mesure \see InfluxDB_addMeasure.
 *
 * 	Les précisions disponibles sont:
 * 		-  S:	seconde
 * 		- MS:	milliseconde
 * 		- US:	microsecode
 * 		- NS:	nanoseconde
 *
 * @param clt			Client InfluxDB.
 * @param precision		Time unit.
 */
void InfluxDB_setPrecision( InfluxDBClient* clt, InfluxDB_Prec_t precision )
{
	if( !clt ) return;

	clt->precision = precision;
}
/*!
 * \brief Sets min. measurements number level.
 *
 *	Fixe le nombre minimum de mesures avant envoi effectif.
 * Valeur par défaut: 0, pas de seuil.
 *
 * @param clt		Client InfluxDB.
 * @param min		Nombre de mesures min. avant envoi.
 */
void InfluxDB_setMinMeasures( InfluxDBClient* clt, size_t min )
{
	if( !clt ) return;

	clt->min_mesures = min;
}
/*!
 * \brief Sets max. time interval..
 *
 *	Fixe l'intervalle maximum entre deux envois effectifs.
 * Valeur par défaut: 0, pas de seuil.
 *
 * @param clt		Client InfluxDB.
 * @param min		Intervalle maxi. avant envoi.
 */
void InfluxDB_setMaxInterval( InfluxDBClient* clt, size_t max )
{
	if( !clt ) return;

	clt->max_temps = max;
}
/*!
 * \brief	Ajout d'un tag ou d'un field.
 *
 *	Ajout d'un tag ou d'un field de type string dans la bonne liste.
 *
 * @param tagl		Liste de tags (ou de fields).
 * @param nom		Nom du tag.
 * @param valeur	Valeur du tag ou du field.
 */
static void addRealTag( nvlist_t* tagl, char* nom, char* valeur )
{
	nvlist_add_string( tagl, nom, valeur );
}
/*!
 * \brief	Ajout d'un tag ou d'un field.
 *
 *	Ajout d'un tag ou d'un field de type int64_t dans la bonne liste.
 *
 * @param tagl		Liste de tags (ou de fields).
 * @param nom		Nom du tag.
 * @param valeur	Valeur du tag ou du field.
 */
static void addRealTagInt( nvlist_t* tagl, char* nom, int64_t valeur )
{
	nvlist_add_int64( tagl, nom, valeur );
}
/*!
 * \brief	Ajout d'un tag ou d'un field.
 *
 *	Ajout d'un tag ou d'un field de type int64_t dans la bonne liste.
 *
 * @param tagl		Liste de tags (ou de fields).
 * @param nom		Nom du tag.
 * @param valeur	Valeur du tag ou du field.
 */
static void addRealTagUInt( nvlist_t* tagl, char* nom, uint64_t valeur )
{
	nvlist_add_uint64( tagl, nom, valeur );
}
/*!
 * \brief	Ajout d'un tag ou d'un field.
 *
 *	Ajout d'un tag ou d'un field de type int64_t dans la bonne liste.
 *
 * @param tagl		Liste de tags (ou de fields).
 * @param nom		Nom du tag.
 * @param valeur	Valeur du tag ou du field.
 */
static void addRealTagDouble( nvlist_t* tagl, char* nom, double valeur )
{
	nvlist_add_double( tagl, nom, valeur );
}
/*!
 * \brief	Ajout d'un tag global.
 *
 * @param clt		Client InfluxDB.
 * @param nom		Nom du tag.
 * @param valeur	Valeur du tag.
 */
void InfluxDB_addGlobalTag( InfluxDBClient* clt, char* nom, char* valeur )
{
	if( !clt ) return;

	char* e_nom = escapeKey(nom, FALSE);
	char* e_valeur = escapeKey(valeur, TRUE);
	addRealTag( clt->tags_commun, nom, valeur );

	g_free( e_nom );
	g_free( e_valeur );
}
/*!
 * Is GlobalTag empty ?
 *
 * @param clt		Client InfluxDB.
 *
 * @return TRUE or FALSE
 */
gboolean InfluxDBGlobalTag_empty( InfluxDBClient* clt )
{
	if( !clt ) return TRUE;

	return nvlist_empty( clt->tags_commun );
}
/*!
 * \brief	Ajout d'un tag.
 *
 *	Ajoute un tag dont la valeur est une chaîne de caractère.
 * Les caractères spéciaux sont protégés.
 *
 * @param clt		Client InfluxDB.
 * @param nom		Nom du tag.
 * @param valeur	Valeur du tag (string).
 */
void InfluxDB_addTag( InfluxDBClient* clt, char* nom, char* valeur )
{
	if( !clt ) return;

	char* e_nom = escapeKey(nom, FALSE);
	char* e_valeur = escapeKey(valeur, TRUE);
	addRealTag( clt->tags, e_nom, e_valeur );

	g_free( e_nom );
	g_free( e_valeur );
}
/*!
 * \brief	Ajout d'un tag.
 *
 *	Ajoute un tag dont la valeur est un entier non signé.
 *
 * @param clt		Client InfluxDB.
 * @param nom		Nom du tag.
 * @param valeur	Valeur du tag (uint64_t).
 */
void InfluxDB_addTagUInt( InfluxDBClient* clt, char* nom, uint64_t valeur )
{
	if( !clt ) return;

	char* e_nom = escapeKey(nom, FALSE);
	addRealTagUInt( clt->tags, nom, valeur );

	g_free( e_nom );
}
/*!
 * \brief	Ajout d'un tag.
 *
 *	Ajoute un tag dont la valeur est un entier signé.
 *
 * @param clt		Client InfluxDB.
 * @param nom		Nom du tag.
 * @param valeur	Valeur du tag (int64_t).
 */
void InfluxDB_addTagInt( InfluxDBClient* clt, char* nom, int64_t valeur )
{
	if( !clt ) return;

	char* e_nom = escapeKey(nom, FALSE);
	addRealTagInt( clt->tags, nom, valeur );

	g_free( e_nom );
}
/*!
 * \brief	Ajout d'un tag.
 *
 *	Ajoute un tag dont la valeur est un nombre double precision.
 *
 * @param clt		Client InfluxDB.
 * @param nom		Nom du tag.
 * @param valeur	Valeur du tag (double).
 */
void InfluxDB_addTagDouble( InfluxDBClient* clt, char* nom, double valeur )
{
	if( !clt ) return;

	char* e_nom = escapeKey(nom, FALSE);
	addRealTagDouble( clt->tags, nom, valeur );

	g_free( e_nom );
}
/*!
 * \brief	Ajout d'un field.
 *
 *	Ajout d'un field de valeur string dans la liste des fields
 *
 * @param clt		Client InfluxDB.
 * @param nom		Nom du tag.
 * @param valeur	Valeur du field (string).
 */
void InfluxDB_addField( InfluxDBClient* clt, char* nom, char* valeur )
{
	if( !clt ) return;

	char* e_nom = escapeKey(nom, FALSE);
	char* e_value = escapeValue( valeur );
	addRealTag( clt->fields, e_nom, e_value );

	g_free( e_nom );
	g_free( e_value );
}
/*!
 * \brief	Ajout d'un field.
 *
 *	Ajout d'un field de valeur entière non signée dans la liste des fields.
 *
 * @param clt		Client InfluxDB.
 * @param nom		Nom du tag.
 * @param valeur	Valeur du field (uint64_t).
 */
void InfluxDB_addUIntField( InfluxDBClient* clt, char* nom, uint64_t valeur )
{
	if( !clt ) return;

	char* e_nom = escapeKey(nom, FALSE);
	addRealTagUInt( clt->fields, e_nom, valeur );

	g_free( e_nom );
}
/*!
 * \brief	Ajout d'un field.
 *
 *	Ajout d'un field de valeur entière signée dans la liste des fields
 *
 * @param clt		Client InfluxDB.
 * @param nom		Nom du tag.
 * @param valeur	Valeur du field (int64_t).
 */
void InfluxDB_addIntField( InfluxDBClient* clt, char* nom, int64_t valeur )
{
	if( !clt ) return;

	char* e_nom = escapeKey(nom, FALSE);
	addRealTagInt( clt->fields, e_nom, valeur );

	g_free( e_nom );
}
/*!
 * \brief	Ajout d'un field.
 *
 *	Ajout d'un field de valeur double dans la liste des fields
 *
 * @param clt		Client InfluxDB.
 * @param nom		Nom du tag.
 * @param valeur	Valeur du field (double).
 */
void InfluxDB_addDoubleField( InfluxDBClient* clt, char* nom, double valeur )
{
	if( !clt ) return;

	char* e_nom = escapeKey(nom, FALSE);
	addRealTagDouble( clt->fields, e_nom, valeur );

	g_free( e_nom );
}
/*!
 * \brief	expand tags to line protocol.
 *
 * @param buffer
 * @param list
 * @param sep
 *
 * @return
 */
static GString* append_tags( GString* buffer, nvlist_t* list, char sep )
{
	nvpair_t*	elem = NULL;

	while( (elem = nvlist_next_nvpair( list, elem ) ) != NULL ) {
		switch( nvpair_type( elem ) ) {
		case DATA_TYPE_STRING:
		{
			char*	value;
			(void)nvpair_value_string( elem, &value );
			g_string_append_printf( buffer, "%c%s=%s", sep, nvpair_name(elem), (char*)value );
			break;
		}
		case DATA_TYPE_INT64:
		{
			int64_t	value;
			(void)nvpair_value_int64( elem, &value );
			g_string_append_printf( buffer, "%c%s=%lldi", sep, nvpair_name(elem), (int64_t)value );
			break;
		}
		case DATA_TYPE_UINT64:
		{
			uint64_t	value;
			(void)nvpair_value_uint64( elem, &value );
			g_string_append_printf( buffer, "%c%s=%llui", sep, nvpair_name(elem), (uint64_t)value );
			break;
		}
		case DATA_TYPE_DOUBLE:
		{
			double	value;
			(void)nvpair_value_double( elem, &value );
			g_string_append_printf( buffer, "%c%s=%g", sep, nvpair_name(elem), (double)value );
			break;
		}
		default:
			(void) printf("Not implemented type %d for %s\n", nvpair_type(elem), nvpair_name(elem) );
		}
		sep = ',';
	}
	return buffer;
}
/*!
 * \brief Append timestamp.
 *
 *	Defaults to seconds.
 *
 * @param buffer
 * @param precision
 *
 * @return buffer;
 */
static GString* append_date( GString* buffer, int precision )
{
	struct timeval tps;

	gettimeofday( &tps, NULL );
	switch( precision ) {
	case 9:
		g_string_append_printf( buffer, " %lu%lu000\n", tps.tv_sec, tps.tv_usec );
		break;
	case 6:
		g_string_append_printf( buffer, " %lu%lu\n", tps.tv_sec, tps.tv_usec );
		break;
	case 3:
		g_string_append_printf( buffer, " %lu%lu\n", tps.tv_sec, tps.tv_usec/1000 );
		break;
	default:
		g_string_append_printf( buffer, " %lu\n", tps.tv_sec );
		break;
	}

	return buffer;
}
/*!
 * \brief	Ajout d'une mesure.
 *
 *	Génère une mesure en format 'line', stockée dans un buffer jusqu'à écriture.
 *
 * @param clt		Client InfluxDB.
 * @param nom		Nom de la mesure.
 */
void InfluxDB_addMeasure( InfluxDBClient* clt, char* nom )
{
	if( !clt ) return;

	g_string_append_printf( clt->buffer, "%s", nom );

	if( InfluxDBGlobalTag_empty(clt)==FALSE ) {
		clt->buffer = append_tags( clt->buffer, clt->tags_commun, ',' );
	}
	clt->buffer = append_tags( clt->buffer, clt->tags, ',' );
	clt->buffer = append_tags( clt->buffer, clt->fields, ' ' );
	clt->buffer = append_date( clt->buffer, clt->precision );
	clt->nb_mesures++;

	reset_tags_and_fields( clt );
}
/*!
 * \brief		Convert measurements to string.
 *
 * @param clt	Client InfluxDB.
 *
 * @return	Measurements en protocole ligne.
 */
const char const* InfluxDB_to_string( InfluxDBClient* clt )
{
	return clt->buffer->str;
}
/*!
 * \brief Effectuer une requête à InfluxDB avec la méthode POST.
 *
 *	La requête pointée par "requete" est passée à InfluxDB via une méthode POST.
 * La réponse est rendue dans une zone allouée par la fonction à libérer à l'aide de
 * \see free().
 *
 * @param clt		Client InfluxDB.
 * @param requete	Le texte de la requête INFLUXQL.
 * @param resp		Le contenu de la réponse.
 *
 * @return	Le code retour fourni par InfluxDB (ex 200).
 */
int InfluxDB_postQuery( InfluxDBClient* clt, char* requete, char** resp )
{
	GString*	chn = g_string_new( clt->urlp );
	struct curl_slist*	headers = NULL;

	if( clt->version == 1 ) {
		g_string_append_printf( chn, "/query?db=%s", clt->base_or_bucket );
		if( clt->user_or_org ) {
			g_string_append_printf( chn, "&u=%s", clt->user_or_org );
			if( clt->pass_or_token ) {
				g_string_append_printf( chn, "&p=%s", clt->pass_or_token );
			}
		}
		//
		//	Headers.
		//
		headers = curl_slist_append( headers, "Content-Type:application/x-www-form-urlencoded" );
		curl_easy_setopt( clt->curl, CURLOPT_HTTPHEADER, headers );
	} else {
		// TODO intégrer la retention_policy
		g_string_append_printf( chn, "/query?database=%s&org=%s", clt->base_or_bucket, clt->user_or_org );
		//
		//	Headers + Authorization token.
		//
		struct curl_slist*	headers = NULL;
		char				auth[512];
		headers = curl_slist_append( headers, "Content-Type:application/json" );
		snprintf( auth, sizeof(auth), "Authorization: Token %s", clt->pass_or_token );
		headers = curl_slist_append( headers, auth );
		curl_easy_setopt( clt->curl, CURLOPT_HTTPHEADER, headers );

	}
	GString*	body = g_string_new(NULL);
	g_string_append_printf( body, "q=%s", requete );

	if( clt->chunks->size ) {
		free( clt->chunks->memory );
		clt->chunks->size = 0;
		clt->chunks->memory = NULL;
	}
	int ret = query( clt->curl, chn->str, CURLOPT_POST, body, clt->chunks, WriteMemoryCallback  );

	g_string_free( chn, TRUE );
	g_string_free( body, TRUE );
	curl_slist_free_all( headers );

	if( resp ) {
		*resp = malloc( clt->chunks->size+1 );
		memcpy( *resp, clt->chunks->memory, clt->chunks->size+1 );
	}

	return ret;
}
/*!
 * \brief Effectuer une requête à InfluxDB avec la méthode GET.
 *
 *	La requête pointée par "requete" est passée à InfluxDB via une méthode POST.
 * La réponse est rendue dans une zone allouée par la fonction à libérer à l'aide de
 * \see free().
 *
 * @param clt		Client InfluxDB.
 * @param requete	Le texte de la requête INFLUXQL.
 * @param resp		Le contenu de la réponse.
 *
 * @return	Le code retour fourni par InfluxDB (ex 200).
 */
int InfluxDB_getQuery( InfluxDBClient* clt, char* requete, char** resp )
{
	GString*	chn = g_string_new( clt->urlp );
	struct curl_slist*	headers = NULL;

	if( clt->version == 1 ) {
		g_string_append_printf( chn, "/query?db=%s", clt->base_or_bucket );
		if( clt->user_or_org ) {
			g_string_append_printf( chn, "&u=%s", clt->user_or_org );
			if( clt->pass_or_token ) {
				g_string_append_printf( chn, "&p=%s", clt->pass_or_token );
			}
		}
		//
		//	Headers.
		//
		headers = curl_slist_append( headers, "Content-Type:application/x-www-form-urlencoded" );
		curl_easy_setopt( clt->curl, CURLOPT_HTTPHEADER, headers );
	} else {
		// TODO intégrer la retention_policy
		g_string_append_printf( chn, "/query?database=%s&org=%s", clt->base_or_bucket, clt->user_or_org );
		//
		//	Headers + Authorization token.
		//
		struct curl_slist*	headers = NULL;
		char				auth[512];
		headers = curl_slist_append( headers, "Content-Type:application/json" );
		snprintf( auth, sizeof(auth), "Authorization: Token %s", clt->pass_or_token );
		headers = curl_slist_append( headers, auth );
		curl_easy_setopt( clt->curl, CURLOPT_HTTPHEADER, headers );

	}
	g_string_append( chn, "&q=" );
	char* url_encoded_output = curl_easy_escape( clt->curl, requete, 0 );
	g_string_append( chn, url_encoded_output );

	if( clt->chunks->size ) {
		free( clt->chunks->memory );
		clt->chunks->size = 0;
		clt->chunks->memory = NULL;
	}
	int ret = query( clt->curl, chn->str, CURLOPT_HTTPGET, NULL, clt->chunks, WriteMemoryCallback  );

	curl_free( url_encoded_output );
	g_string_free( chn, TRUE );
	curl_slist_free_all( headers );

	if( resp ) {
		*resp = malloc( clt->chunks->size+1 );
		memcpy( *resp, clt->chunks->memory, clt->chunks->size+1 );
	}

	return ret;
}
/*!
 * \brief	Fonction pour traiter une requête selon le protocole HTTP.
 *
 * @param client	Client HTTP.
 * @param url		URL à requêter
 * @param method	POST ou GET.
 * @param body		Données en mode POST.
 * @param chunk		Structure de bloc pour traiter la réponse.
 * @param cb		Traitement des blocs reçus.
 *
 * @return -1 ou HTTP status code.
 */
static int query( CURL* client, char* url, int method, GString* body,
		struct chunk* chunk, size_t (*cb)( void *contents, size_t size, size_t nmemb, void *userp ) )
{
	CURLcode res = curl_easy_setopt( client, CURLOPT_URL, url );
	if( res != CURLE_OK ) {
		Error( stderr, "Can't set URL in %s: %s\n", __func__, curl_easy_strerror(res) );
		return -1;
	}
	res = curl_easy_setopt( client, method, 1L );
	if( res != CURLE_OK ) {
		Error( stderr, "Can't set method in %s: %s\n", __func__, curl_easy_strerror(res) );
		return -1;
	}

	if( body && method == CURLOPT_POST ) {
		res = curl_easy_setopt( client, CURLOPT_POSTFIELDSIZE, body->len );
		res = curl_easy_setopt( client, CURLOPT_POSTFIELDS, body->str );

		if( res != CURLE_OK ) {
			Error( stderr, "Can't set POSTFIELDS in %s: %s\n", __func__, curl_easy_strerror(res) );
			return -1;
		}
	}

	res = curl_easy_setopt( client, CURLOPT_WRITEFUNCTION, cb );
	if( res != CURLE_OK ) {
		Error( stderr, "Can't set WRITEFUNCTION in %s: %s\n", __func__, curl_easy_strerror(res) );
		return -1;
	}
	res = curl_easy_setopt( client, CURLOPT_WRITEDATA, chunk );
	if( res != CURLE_OK ) {
		Error( stderr, "Can't set WRITEDATA in %s: %s\n", __func__, curl_easy_strerror(res) );
		return -1;
	}

	res = curl_easy_perform( client );
	if( res != CURLE_OK ) {
		Error( stderr, "Can't exec GET/POST in %s: %s\n", __func__, curl_easy_strerror(res) );
		return -1;
	}

	long response_code;
	res = curl_easy_getinfo( client, CURLINFO_RESPONSE_CODE, &response_code );
	if( res != CURLE_OK ) {
		Error( stderr, "Can't get RESPONSE CODE in %s: %s\n", __func__, curl_easy_strerror(res) );
		return -1;
	}

	return (int)response_code;
}

/*!
 * \brief	Send datas with V1 protocol.
 *
 *	Send to /write?db={database}[&user={user}[&password={pass}]][&precision={precision}]
 *
 * - La précision est indiquée si elle est différente de NS.
 * - Les informations "user" et "password" sont optionnelles.
 * - Les données sont expédiées en format POST.
 *
 * @param clt	Client InfluxDB.
 *
 * @return
 */
static int send_v1( InfluxDBClient* clt )
{
	GString*	chn = g_string_new( clt->urlp );

	g_string_append_printf( chn, "/write?db=%s", clt->base_or_bucket );
	if( clt->user_or_org ) {
		g_string_append_printf( chn, "&u=%s", clt->user_or_org );
		if( clt->pass_or_token ) {
			g_string_append_printf( chn, "&p=%s", clt->pass_or_token );
		}
	}
	if( clt->precision != NS ) {
		g_string_append_printf( chn, "&precision=%s", clt->precision==S?"s":clt->precision==MS?"ms":"us" );
	}
	//
	//	Headers + Authorization token.
	//
	struct curl_slist*	headers = NULL;

	headers = curl_slist_append( headers, "Content-Type:text/plain" );
	curl_easy_setopt( clt->curl, CURLOPT_HTTPHEADER, headers );

	if( clt->chunks->size ) {
		free( clt->chunks->memory );
		clt->chunks->size = 0;
		clt->chunks->memory = NULL;
	}
	int ret = query( clt->curl, chn->str, CURLOPT_POST, clt->buffer, clt->chunks, WriteMemoryCallback );

	g_string_free( chn, TRUE );
	curl_slist_free_all( headers );

	return ret;
}
/*!
 * \brief	Send datas with V2 protocol.
 *
 *	Send to /api/v2/write?bucket={bucket}&org={organization}[&precision={precision}].
 *
 * - La précision est indiquée si elle est différente de NS.
 * - Le token d'authorisation est passé dans une entête.
 * - Les données sont expédiées en format POST.
 *
 * @param clt	Client InfluxDB.
 *
 * @return
 */
static int send_v2( InfluxDBClient* clt )
{
	GString*	chn = g_string_new( clt->urlp );

	g_string_append_printf( chn, "/api/v2/write?bucket=%s", clt->base_or_bucket );
	g_string_append_printf( chn, "&org=%s", clt->user_or_org );
	if( clt->precision != NS ) {
		g_string_append_printf( chn, "&precision=%s", clt->precision==S?"s":clt->precision==MS?"ms":"us" );
	}
	//
	//	Headers + Authorization token.
	//
	struct curl_slist*	headers = NULL;
	char				auth[512];
	headers = curl_slist_append( headers, "Content-Type:text/plain" );
	snprintf( auth, sizeof(auth), "Authorization: Token %s", clt->pass_or_token );
	headers = curl_slist_append( headers, auth );
	curl_easy_setopt( clt->curl, CURLOPT_HTTPHEADER, headers );

	if( clt->chunks->size ) {
		free( clt->chunks->memory );
		clt->chunks->size = 0;
		clt->chunks->memory = NULL;
	}
	int ret = query( clt->curl, chn->str, CURLOPT_POST, clt->buffer, clt->chunks, WriteMemoryCallback );

	g_string_free( chn, TRUE );
	curl_slist_free_all( headers );

	return ret;
}
/*!
 * \brief Envoi des mesures à la base.
 *
 * @param clt	Client InfluxDB.
 *
 * @return	0: intervalle max. ou nb de mesures min. non atteint, 204: succès, -2 version inconnue, sinon HTTP status code.
 */
int InfluxDB_write( InfluxDBClient* clt )
{
	int ret = 0;

	int check_nb = ( clt->min_mesures <= clt->nb_mesures );
	int check_tps = clt->max_temps <= ( time(NULL) - clt->last_send );
	if( ! check_nb && ! check_tps ) return 0;

	if( clt->version == v1 ) {
		ret = send_v1( clt );
	} else if( clt->version == v2 ) {
		ret = send_v2( clt );
	} else {
		ret = -2;
	}

	if( ret >= 0  ) {
		clt->last_send = time(NULL);
		clt->nb_mesures = 0;
		clt->buffer = g_string_truncate( clt->buffer, 0 );
	}

  	return ret;
}
/*!
 * \brief	Get response body.
 *
 *	Retourne le contenu du buffer de réponse immédiatement après un QUERY ou un WRITE.
 *	Ce buffer ne doit pas être modifié.
 *
 * @param clt	Client InfluxDB.
 * @param size	Si non nul, emplacement où stocker la taille de la réponse (en octents).
 *
 * @return	Le buffer de réponse.
 */
const char const * InfluxDB_get_response( InfluxDBClient* clt, size_t* size )
{
	if( size ) *size = clt->chunks->size;

	return clt->chunks->memory;
}
