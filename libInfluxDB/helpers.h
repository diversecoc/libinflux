/*!
 * \file helpers.c
 *
 * \brief API de communication avec InfluxDB
 *
 *	En-tête API client en écriture pour InfluxDB
 *
 * @(#) $Id$ Ecoc
 *
 * \author Ecochard Jean-Paul <jp.ecoc@free.fr>
 * \date   mer. juin 16 13:54:24 CET 2021
 */
#ifndef HELPERS_H_
#define HELPERS_H_

char* escapeKey( char* key, gboolean escapeEqual );
char* escapeValue(const char *value);
char* urlEncode( const char* src );
void Error( FILE* out, char* fmt, ... );

#endif /* HELPERS_H_ */
