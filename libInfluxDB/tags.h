/*!
 * \file tags.h
 *
 * \brief API de communication avec InfluxDB
 *
 *	Tags and fields management.
 *
 * @(#) $Id$ Ecoc
 *
 * \author Ecochard Jean-Paul <jp.ecoc@free.fr>
 * \date   ven. mars. 2 18:54:24 CET 2018
 */
#ifndef TAGS_H_
#define TAGS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "libnvpair.h"
/*!
 * \brief Magic Header for tag structure
 */
#define TAG_MAGIC	"TAG V1"
/*!
 * \brief Tags list type.
 */
typedef nvlist_t TagList_t;
/*!
 * \brief Tag list entry.
 */
typedef nvpair_t TagValue_t;
/*!
 * \brief	Tags collection.
 *
 * Data structure used for InfluxDB tags and fields collection. A tag collection is a growing array of
 * couple (variable,value).
 * 	- Variable is unique within a collection.
 * 	- If a variable already exists, the value is updated.
 * 	- CAUTION. Although same variable name may exists in two (or more) collections, only one MUST be present
 * 	in InfluDB request; This in not enforced.
 */
typedef struct tag_s {
	char		version[8];		/*!< Tag collection magic. */
	boolean_t	init_done;		/*!< Tag structure initialized. */
	u_int32_t	nb;				/*!< Number of tags. */
	TagList_t*	tags;			/*!< Growing collection of tags. */
} Tag_t;

Tag_t*		new_tag( void );
int			add_tag( Tag_t *tag, const char *nom, u_int32_t size, const void *value );
void		truncate_tag( Tag_t *tag );
void		free_tag( Tag_t *tag );
char*		tag_name( TagValue_t* elem );
char*		tag_value( TagValue_t* elem );
TagValue_t*	next( Tag_t* tag, TagValue_t* elem );

void		dump_tags( Tag_t* tag, int indent );

//TagValue_t	*findByName( Tag_t *tag, char *name );
//TagValue_t	*findByValue( Tag_t *tag, uint32_t size, const void *value );

#endif /* TAGS_H_ */
