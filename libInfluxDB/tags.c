/*!
 * \file tags.c
 *
 * \brief API de communication avec InfluxDB
 *
 *	API client en écriture pour InfluxDB
 *
 *	Various functions related to tags or fields management.
 *
 * @(#) $Id$ Ecoc
 *
 * \author Ecochard Jean-Paul <jp.ecoc@free.fr>
 * \date   ven. mars. 2 18:54:24 CET 2018
 */
#include "tags.h"

#include <time.h>

/*!
 * \brief	Create an empty tag structure.
 *
 *	Allocate an empty tag structure for (key,value) pairs.
 *
 * \return The new Tag_t structure or NULL if allocation failed.
 */
Tag_t*	new_tag( void )
{
	Tag_t	*n = malloc( sizeof(Tag_t) );
	if( n ) {
		strcpy( n->version, TAG_MAGIC );
		n->nb = 0;
		n->tags = NULL;
	}
	nvlist_alloc( &n->tags, NV_UNIQUE_NAME, 0 );
	n->init_done = B_TRUE;
	return n;
}
/*!
 * \brief	Free tag list resources.
 *
 *	Tag values and tag structure are both freed.
 *
 * \param tag	Tag list to be freed.
 *
 * \return void
 */
void free_tag( Tag_t *tag )
{
	if( !tag || strcmp( tag->version, TAG_MAGIC ) || tag->init_done != B_TRUE ) return;

	nvlist_free( tag->tags );
	free(  tag->tags );
	tag->tags	= NULL;
	tag->nb		= 0;
	free( tag );
}
/*!
 * \brief	Truncate tag list.
 *
 *	Delete all tags and their values.
 *
 * \param tag	List to be truncated.
 *
 * \return void
 */
inline void truncate_tag( Tag_t *tag )
{
	free_tag( tag );
}
/*!
 * \brief Add a (key,value) pair to tag list.
 *
 *	Add (key,value) pair into tag list. If 'key' already exists, 'value' is updated.
 * 'value' is a 'size' bytes stream, so any type can be stored.
 *
 * When used with C strings, remember to add ending nul char.
 *
 *	If 'key' or 'value' allocation failed, 'tag' is left unchanged and an error is returned.
 *
 * \param tag	Tag list
 * \param key	Tag name
 * \param size	Value size (in byte).
 * \param value	Value (size byte long).
 *
 * \return 0 insert OK, 1 erreur inserting value, 2 erreur inserting key, -1 invalid tag or NULL
 */
int add_tag( Tag_t* tag, const char* key, u_int32_t size, const void* value )
{
	if( !tag || strcmp( tag->version, TAG_MAGIC ) ) return -1;
	if( tag->init_done != B_TRUE ) {
		nvlist_alloc( &tag->tags, NV_UNIQUE_NAME, 0 );
		tag->init_done = B_TRUE;
	}

	nvlist_add_string( tag->tags, key, value );
	return 0;
}

TagValue_t* next( Tag_t* tag, TagValue_t* elem ) {
	return nvlist_next_nvpair( tag->tags, elem );
}

char* tag_name( TagValue_t* elem ) {
	return nvpair_name(elem);
}

char* tag_value( TagValue_t* elem ) {
	char*	value = NULL;
	(void)nvpair_value_string( elem, &value );

	return value;
}
/*!
 * \brief Dump tag list.
 *
 * Dumps key/pair values
 *
 * \param tag		Tag list to dump.
 * \param indent	Indent size.
 */
void dump_tags( Tag_t* tag, int indent )
{
	if( !tag || strcmp( tag->version, TAG_MAGIC ) || tag->init_done != B_TRUE ) return;

	nvpair_t*	elem = NULL;
	boolean_t	bool_value;

	dump_nvlist( tag->tags, 2 );
}
