/*!
 * \file helpers.c
 *
 * \brief API de communication avec InfluxDB
 *
 *	En-tête API client en écriture pour InfluxDB
 *
 * @(#) $Id$ Ecoc
 *
 * \author Ecochard Jean-Paul <jp.ecoc@free.fr>
 * \date   lun. juin. 14 10:34:18 CET 2021
 */

#include <stdio.h>
#include <stdarg.h>

#include <glib.h>
#include <gmodule.h>
/*!
 * \brief Escape tag/field key.
 *
 * @param key			Tag/field key
 * @param escapeEqual	True if '=' must be escaped.
 *
 * @return Escaped string (must be freed).
 */
char* escapeKey( char* key, gboolean escapeEqual ) {
	GString* ret;

	ret = g_string_sized_new( strlen(key) + 5 );	//5 is estimate of  chars needs to escape,

	char* c = key;
	while( *c ) {
		switch( *c ) {
		case '\r':
		case '\n':
		case '\t':
		case ' ':
		case ',':
			ret = g_string_append_c( ret, '\\' );
			break;
		case '=':
			if(escapeEqual) {
				ret = g_string_append_c( ret, '\\' );
			}
			break;
		}
		ret = g_string_append_c( ret, *c );
		c++;
	}
	return g_string_free( ret, FALSE );
}
/*!
 * Escape tag value.
 *
 * @param value	Tag value.
 *
 * @return Escaped string (must be freed).
 */
char* escapeValue(const char *value)
{
	GString*	ret;

	int len = strlen( value );
	ret = g_string_sized_new( len + 5 );	//5 is estimate of  chars needs to escape,

	ret = g_string_append_c( ret, '\"' );
	for( int i=0; i<len; i++ ) {
		switch( value[i] ) {
		case '\\':
		case '\"':
			ret = g_string_append_c( ret, '\\' );
			break;
		}

		ret = g_string_append_c( ret, value[i] );
	}
	ret = g_string_append_c( ret, '\"' );

	return g_string_free( ret, FALSE );
}

static char invalidChars[] = "$&+,/:;=?@ <>#%{}|\\^~[]`";

static char hex_digit(char c) {
    return "0123456789ABCDEF"[c & 0x0F];
}
/*!
 * \brief Encode invalid chars in url string.
 *
 * @param src	Url
 *
 * @return Encoded string (must be freed).
 */
char* urlEncode( const char* src )
{
	int n=0;
	char	c;
	char*	s = (char *)src;

	while( (c = *s++) ) {
		if( strchr( invalidChars, c ) ) {
			n++;
		}
	}
	GString* ret;
	ret = g_string_sized_new( strlen(src) + 2*n + 1 );
	s = (char *)src;
	while( (c = *s++) ) {
		if( strchr( invalidChars,c ) ) {
			ret = g_string_append_c( ret, '%' );
			ret = g_string_append_c( ret, hex_digit(c >> 4) );
			ret = g_string_append_c( ret, hex_digit(c) );
		} else ret = g_string_append_c( ret, c );
	}

	return g_string_free( ret, FALSE );
}
/*!
 *	\brief Message d'erreur.
 *
 * @param out	Flus de sortie.
 * @param fmt	Format du message (voir format printf).
 */
void Error( FILE* out, char* fmt, ... )
{
	va_list ap;

	va_start( ap, fmt );
	vfprintf( out, fmt, ap );
	va_end( ap );
}
#ifdef TEST
int main( int argc, char* argv[] )
{
	char* url = urlEncode( "$&+,/:;=?@ <>#%{}|\\^~[]`" );

	fprintf( stderr, "%s\n", url );

	g_free( url );

	return 0;
}
#endif
