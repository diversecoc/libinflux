/*
 * exempleV1.c
 *
 *  Created on: 18 juin 2021
 *      Author: ecoc
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include "influx.h"

#define URL		"http://friolin:8086"
#define USER	"Tests"
#define PASS	"Tests"
#define BASE	"Tests"

int main( int argc, char *argv[] )
{
	InfluxDBClient* clt = InfluxDBClientNew();
	InfluxDB_setConnectionParamsV1( clt, URL, USER, PASS, BASE );

	InfluxDB_addGlobalTag( clt, "host", "turia" );
	InfluxDB_addGlobalTag( clt, "programme", "exempleV1" );
	InfluxDB_setMinMeasures( clt, 10 );
	InfluxDB_setMaxInterval( clt, 300 );

	srand48( time(NULL) );
	for( int i=1; i != 3600; i++ ) {
		double dval = drand48();
		long	uval = lrand48();
		long	lval = mrand48();
		InfluxDB_addDoubleField( clt, "double", 100*dval );
		InfluxDB_addUIntField( clt, "positif", (uint64_t)uval );
		InfluxDB_addIntField( clt, "entier", (int64_t)lval );

		InfluxDB_addMeasure( clt, "tests" );

		fprintf( stderr, "." );
		int ret = InfluxDB_write( clt );
		if( ret && ret != 204 ) {
			fprintf( stderr, "\nret: %d\n%s\n", ret, InfluxDB_get_response( clt, NULL ) );
		}
		if( ret == 204 ) {
			fprintf( stderr, ": 204\n" );
		}
		sleep( 10 );
	}

	InfluxDBClientDestroy( clt );

}
