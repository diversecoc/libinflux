/*!
 * \file exempleQuery.c
 *
 * \brief
 *
 *
 *
 * @(#) $Id$ Ecoc
 *
 * \author Ecochard Jean-Paul <jp.ecoc@free.fr>
 * \date   jeu. mars 28 15:13:29 CET 2024
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include "influx.h"

int main( int argc, char *argv[] )
{
	if( argc != 6 ) {
		fprintf( stderr,  "Usage: %s URL User Pass Base \"query\"\n", argv[0] );
		return EXIT_FAILURE;
	}
	char* URL = argv[1];
	char* USER = argv[2];
	char* PASS = argv[3];
	char* BASE = argv[4];
	char* Query = argv[5];

	InfluxDBClient* clt = InfluxDBClientNew();
	InfluxDB_setConnectionParamsV1( clt, URL, USER, PASS, BASE );

	char* resp = NULL;
	int ret = InfluxDB_getQuery( clt, "SHOW DATABASES", &resp );
	if( ret != 200 ) {
		fprintf( stderr, "Erreur: %d\n", ret );
	} else {
		fprintf( stderr, "resp = %s\n", resp );
	}
	free( resp );

	ret = InfluxDB_postQuery( clt, Query, &resp );
	if( ret != 200 ) {
		fprintf( stderr, "Erreur: %d\n", ret );
	} else {
		fprintf( stderr, "resp = %s\n", resp );
	}
	free( resp );
}
