/*
 * exempleV2.c
 *
 *  Created on: 18 juin 2021
 *      Author: ecoc
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include "influx.h"

#define URL		"http://localhost:8086"
#define ORG		"Tests"
#define BUCKET	"Tests"
#define TOKEN	"aqjgYON3NzcfvMJ6xnbH4ANds7LWqaOKFvx-fb7iNJxSIIZBk8t-XuUpK6kc9yfAX1JxjuiPpzTqFrnxjl-KnQ=="

int main( int argc, char *argv[] )
{
	InfluxDBClient* clt = InfluxDBClientNew();
	InfluxDB_setConnectionParamsV2( clt, URL, ORG, BUCKET, TOKEN );

	InfluxDB_addGlobalTag( clt, "host", "turia" );
	InfluxDB_addGlobalTag( clt, "programme", "exempleV2" );

	srand48( time(NULL) );
	for( int i=1; i != 3600; i++ ) {
		double dval = drand48();
		long	uval = lrand48();
		long	lval = mrand48();
		InfluxDB_addDoubleField( clt, "double", 100*dval );
		InfluxDB_addUIntField( clt, "positif", (uint64_t)uval );
		InfluxDB_addIntField( clt, "entier", (int64_t)lval );

		InfluxDB_addMeasure( clt, "tests" );
		if( (i % 10) == 0 ) {
			fprintf( stderr, ".\n" );
			int ret = InfluxDB_write( clt );
			fprintf( stderr, "%d\n", ret );
			if( ret != 204 ) {
				fprintf( stderr, "%s\n", InfluxDB_get_response( clt, NULL ) );
			}
		} else
			fprintf( stderr, "." );
		sleep( 10 );
	}

	InfluxDBClientDestroy( clt );

}
