/*
 ============================================================================
 Name        : exampleProgram.c
 Author      : ECOCHARD Jean-Paul
 Version     :
 Copyright   : Tous droits réservés
 Description : Uses shared library to print greeting
               To run the resulting executable the LD_LIBRARY_PATH must be
               set to ${project_loc}/libInfluxDB/.libs
               Alternatively, libtool creates a wrapper shell script in the
               build directory of this program which can be used to run it.
               Here the script will be called exampleProgram.
 ============================================================================
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "influx.h"

#define URL		"http://localhost:8086"
#define DB		"test"
#define USER	NULL
#define PWD		NULL

int main(void) {
	// Create connection
	//
	InfluxDBClient* clt = InfluxDBClientNew();
	InfluxDB_setConnectionParamsV1( clt, URL, USER, PWD, DB );
	//
	//	Create comon tags
	//
	InfluxDB_addGlobalTag( clt, "host", "host1" );
	InfluxDB_addGlobalTag( clt, "type", "vm" );
	//
	//	Create some fields
	//
	char str[16];
	for( int i=1; i != 4; i++ ) {
		sprintf( str, "%s_%d", "guest", i );
		InfluxDB_addDoubleField( clt, "load1m", 5.65 );
		InfluxDB_addDoubleField( clt, "load5m", 4.32 );
		InfluxDB_addDoubleField( clt, "load15m",2.10*i );
		InfluxDB_addIntField( clt, "nb_cpus", 8 );
		InfluxDB_addField( clt, "desc", str );
		//
		//	Add measurement "system"
		//
		InfluxDB_addMeasure( clt, "system" );
	}

	fprintf( stderr, "%s", InfluxDB_to_string( clt ) );

	InfluxDBClientDestroy( clt );

	fprintf( stderr, "sizeof(time_t)=%d\n", sizeof(time_t) );

	return 0;
}
